/*
* onur is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* onur is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with onur. If not, see <https://www.gnu.org/licenses/>.
*/

namespace Onur.Commands;

using Onur.Actions;
using Onur.Database;
using Onur.Misc;

///<Summary>
/// Grabs all repositories
///</Summary>
public class Grab
{
    ///<Summary>
    /// Grab by either cloning or pulling a existent repository updates
    ///</Summary>
    public void Run()
    {
        var globals = Globals.GetInstance;
        var klone = new Klone();
        var pull = new Pull();

        var repository = new Repository();
        var allConfigs = repository.Multi();
        if (allConfigs == null)
            return;

        foreach (var config in allConfigs)
        {
            Console.WriteLine($"\n{config.configName.ToLower().ToString()}:");

            foreach (var topic in config.topics)
            {
                Console.WriteLine($" + {topic.Key}");
                foreach (var project in topic.Value)
                {
                    var projectPath = Path.Combine(
                        globals.get("projectsHome"),
                        config.configName.ToLower().ToString(),
                        topic.Key,
                        project.name
                    );

                    PrintInfo(project);

                    if (Directory.Exists(Path.Combine(projectPath, ".git")))
                        pull.Run(project, projectPath);
                    else
                        klone.Run(project, projectPath);
                }
            }
        }
    }

    private void PrintInfo(Domain.Project project)
    {
        Console.WriteLine($" {" ", 2}- {CropTitle(project.name, 35), -45}{CropTitle(project.url, 50), -60}{project.branch}");
    }

    private string CropTitle(string name, int nameLimit) {
        return name.Length <= nameLimit ? name : string.Concat(name.Take(nameLimit)) + "...";
    }
}
