/*
* onur is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* onur is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with onur. If not, see <https://www.gnu.org/licenses/>.
*/

namespace Onur.Commands;

///<Summary>
/// Archives all selected projects
///</Summary>
public class Archive
{
    /// <summary>
    /// Filter by criteria and compress repositories
    /// </summary>
    public void Run(string projectsList)
    {
        var names = projectsList.Split(',');

        Console.WriteLine("Archiving! Using file: {file}");

        foreach (var name in names)
        {
            Console.WriteLine(name);
        }
    }
}
